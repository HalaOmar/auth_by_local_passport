const crypto = require('crypto')

exports.getHashAndSalt = ( password ) => {

    const salt = crypto.randomBytes(32).toString('hex')
    const hashedPassword = crypto.pbkdf2Sync(password,salt,1000,64,"sha1").toString('hex')

    return {
        salt , 
        hashedPassword
    }

}

exports.getHash = ( password , salt ) =>{

    return crypto.pbkdf2Sync(password,salt,1000,64,'sha1').toString('hex')

}