const express = require('express')
const user_controller = require('./user_controller')
const auth_controller = require('../../commons/authcontroller/auth')
const router  = express.Router()



router.route('/register')
.get(user_controller.getRegisterForm)
.post(user_controller.registerUser)

router.route('/login')
.get(user_controller.getRegisterForm)
.post(auth_controller.authenticate)
router.route('/profile')
.get( auth_controller.isAuth, 
      auth_controller.isAdminOrSupervisor)

router.route('/logout')
.get(user_controller.logout)

module.exports = router