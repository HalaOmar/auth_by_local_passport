const { Schema , model } = require('mongoose') 


const UserSchems = new Schema({
    username : {
        type : String ,
        required : true , 
        unique :true
    } , 
    password : {
        type : String , 
        required : true , 
        unique : true

    } ,
    salt : {
        type : String,
        required : true , 
        unique :true
    },
    role : {
        type : String,
        required : true 
    }
} , { timestamps : true})

const User = model('users' , UserSchems)

module.exports = User